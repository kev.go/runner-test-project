# Runner test project

This project is for testing the GitLab CI runner setup and usage as part of working on it.

- shell runner: needed to disable the `FF_RESOLVE_FULL_TLS_CHAIN` feature flag
- docker runner: had to override the `helper_image` to `registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:arm64-latest` because a manifest for the current dev version of the runner didn't seem to exist
